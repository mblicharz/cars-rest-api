import logging
import os
import time

import psycopg2

logger = logging.getLogger()
logger.setLevel(logging.INFO)

DSN = os.environ.get("DATABASE_URL")


def is_db_ready():
    try:
        connection = psycopg2.connect(DSN)
        connection.close()
        return True
    except psycopg2.OperationalError:
        time.sleep(1)
        return False


if __name__ == '__main__':
    start_time = time.time()

    while not is_db_ready():
        logging.warning("Test database isn't ready yet.")
        current_time = time.time()

        if current_time - start_time > 10:
            logging.critical("Test database does not response.")
            break

    logging.warning("Test database is ready!")