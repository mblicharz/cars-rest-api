# Generated by Django 3.1.7 on 2021-02-22 14:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cars_rest', '0004_auto_20210221_1750'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='avg_rating',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='car',
            name='rates_number',
            field=models.IntegerField(default=0),
        ),
    ]
