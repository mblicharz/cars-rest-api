from django.db.models import QuerySet

from cars_rest.models import Car, Rate


def __calculate_average_rating(rates: QuerySet) -> float:
    if not rates:
        return 0.0
    sum_rates = sum([rate.rating for rate in rates])
    return float(sum_rates / len(rates))


def __calculate_rates_count(rates: QuerySet) -> int:
    return len(rates)


def update_rating_statistics_for_car(id: int) -> None:
    car = Car.objects.get(id=id)
    rates = Rate.objects.filter(car=car)
    car.avg_rating = __calculate_average_rating(rates)
    car.rates_number = __calculate_rates_count(rates)
    car.save()
