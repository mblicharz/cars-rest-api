from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


class Rate(models.Model):
    car = models.ForeignKey(
        'Car', related_name='car_id', on_delete=models.CASCADE
    )
    rating = models.PositiveIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1)
        ]
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
