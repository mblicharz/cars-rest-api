from django.db import models


class Car(models.Model):
    make = models.CharField(max_length=255)
    model = models.CharField(max_length=255)
    avg_rating = models.FloatField(null=True, default=None)
    rates_number = models.IntegerField(null=True, default=None)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
