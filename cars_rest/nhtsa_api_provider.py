import json
from typing import List

import requests


class NHTSAUnavailable(Exception):
    ...


class NHTSAUnexpectedData(Exception):
    ...


class CarNotFound(Exception):
    ...


def __url(path: str) -> str:
    return 'https://vpic.nhtsa.dot.gov/api/' + path + '?format=json'


def __find_models_in_nhtsa(make_name: str) -> json:
    url = __url(f'vehicles/GetModelsForMake/{make_name}/')
    try:
        response = requests.get(url)
    except ConnectionError:
        raise NHTSAUnavailable()

    models = json.loads(response.content)
    return models


def get_models_by_make(make_name: str) -> List[tuple]:
    models = __find_models_in_nhtsa(make_name)
    try:
        models_list = models['Results']
    except KeyError:
        raise NHTSAUnexpectedData()

    models = [(item['Make_Name'], item['Model_Name']) for item in models_list
              if item['Make_Name'].lower() == make_name.lower()]

    return models


def get_model(make_name: str, model_name: str) -> tuple:
    models = get_models_by_make(make_name)

    for make, model in models:
        if model_name.lower() == model.lower():
            return make, model

    raise CarNotFound()
