from django.urls import path
from .views import CarsView, RatesView, PopularView

urlpatterns = [
    path('cars/', CarsView.as_view(), name='cars'),
    path('cars/<pk>', CarsView.as_view(), name='cars'),
    path('rate/', RatesView.as_view(), name='rates'),
    path('popular/', PopularView.as_view(), name='popular')
]
