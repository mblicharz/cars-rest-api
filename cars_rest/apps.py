from django.apps import AppConfig


class CarsRestConfig(AppConfig):
    name = 'cars_rest'
