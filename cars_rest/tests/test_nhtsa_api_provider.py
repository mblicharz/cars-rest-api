import unittest

from cars_rest import nhtsa_api_provider as nhtsa


class TestNHTSAAPIProvider(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def test_get_models_by_make(self):
        # Valid make
        models_by_make = nhtsa.get_models_by_make('jaguar')

        self.assertNotEqual(len(models_by_make), 0)

        self.assertIsInstance(models_by_make, list)
        self.assertIsInstance(models_by_make[0], tuple)

        self.assertEqual('jaguar', models_by_make[0][0].lower())

        # Invalid make
        models_by_make = nhtsa.get_models_by_make('fjad')

        self.assertEqual(len(models_by_make), 0)

    def test_get_model(self):
        model = nhtsa.get_model('fiat', 'brava')

        self.assertIsInstance(model, tuple)

        self.assertEqual(model[0].lower(), 'fiat')
        self.assertEqual(model[1].lower(), 'brava')

        self.assertEqual(model[0], 'FIAT')
        self.assertEqual(model[1], 'Brava')
