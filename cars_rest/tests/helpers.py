import statistics
from random import randint
from typing import List

from cars_rest.models import Car, Rate


def generate_cars():
    cars = [
        Car(
            make='Honda',
            model='Civic',
        ),
        Car(
            make='Jaguar',
            model='XE',
        ),
        Car(
            make='Opel',
            model='Astra'
        )
    ]
    return cars


def generate_cars_post_data():
    cars = [
        {
            'make': 'fiat',
            'model': 'brava',
        },
        {
            'make': 'fiat',
            'model': 'strada',
        },
        {
            'make': 'fiat',
            'model': 'ducato',
        },
    ]
    return cars


def generate_rates(car: Car):
    rates = [
        Rate(
            car_id=car.pk,
            rating=randint(1, 5)
        ),
        Rate(
            car_id=car.pk,
            rating=randint(1, 5)
        ),
        Rate(
            car_id=car.pk,
            rating=randint(1, 5)
        ),
        Rate(
            car_id=car.pk,
            rating=randint(1, 5)
        ),Rate(
            car_id=car.pk,
            rating=randint(1, 5)
        ),
        Rate(
            car_id=car.pk,
            rating=randint(1, 5)
        ),
    ]
    return rates


def generate_rates_post_data(car: Car):
    rates = [
        {
            'car_id': car.pk,
            'rating': randint(1, 5),
        },
        {
            'car_id': car.pk,
            'rating': 1,
        },
        {
            'car_id': car.pk,
            'rating': 5,
        },
        {
            'car_id': car.pk,
            'rating': 6,
        },
        {
            'car_id': car.pk,
            'rating': 0,
        },
        {
            'car_id': car.pk,
            'rating': -1,
        },
    ]
    return rates


def count_average(lst: List[int]) -> float:
    return statistics.mean(lst)

