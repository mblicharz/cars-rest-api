import json

from django.test import TestCase, Client
from django.urls import reverse

from cars_rest.models import Car
from cars_rest.tests import helpers


class TestCarsView(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.cars = helpers.generate_cars_post_data()
        self.expected_fields = {
            'id': int,
            'make': str,
            'model': str,
            'avg_rating': type(None),
            'created_at': str,
            'updated_at': str
        }
        [car.save() for car in helpers.generate_cars()]
        self.nonexistent_make = {
            'make': 'fjad',
            'model': 'brava',
        }
        self.nonexistent_model = {
            'make': 'fiat',
            'model': 'bravissimo',
        }

    def test_request_post_on_cars_view(self):
        for car in self.cars:
            response = self.client.post(
                reverse('cars'),
                data=json.dumps(car),
                content_type='application/json'
            )

            self.assertNotEqual(response.status_code, 500)
            self.assertNotEqual(response.status_code, 400)
            self.assertEqual(response.status_code, 201)

            for k, v in response.data.items():
                self.assertIn(k, self.expected_fields.keys())
                self.assertIsInstance(v, self.expected_fields[k])

        # Invalid make
        response = self.client.post(
            reverse('cars'),
            data=json.dumps(self.nonexistent_make),
            content_type='application/json'
        )

        self.assertNotEqual(response.status_code, 500)
        self.assertNotEqual(response.status_code, 201)
        self.assertEqual(response.status_code, 404)

        # Invalid model
        response = self.client.post(
            reverse('cars'),
            data=json.dumps(self.nonexistent_model),
            content_type='application/json'
        )

        self.assertNotEqual(response.status_code, 500)
        self.assertNotEqual(response.status_code, 201)
        self.assertEqual(response.status_code, 404)

    def test_request_get_on_cars_view(self):
        response = self.client.get(reverse('cars'))

        self.assertNotEqual(response.status_code, 500)
        self.assertNotEqual(response.status_code, 404)
        self.assertEqual(response.status_code, 200)

        self.assertNotEqual(len(response.data), 0)

        for item in response.data:
            for k, v in item.items():
                self.assertIn(k, self.expected_fields.keys())
                self.assertIsInstance(v, self.expected_fields[k])

    def test_request_delete_on_cars_view(self):
        car = Car.objects.all()[0]

        response = self.client.delete(reverse('cars', args=[car.pk]))

        self.assertNotEqual(response.status_code, 500)
        self.assertNotEqual(response.status_code, 404)
        self.assertEqual(response.status_code, 204)

        response = self.client.delete(reverse('cars', args=['12345677']))

        self.assertNotEqual(response.status_code, 500)
        self.assertNotEqual(response.status_code, 204)
        self.assertEqual(response.status_code, 404)
