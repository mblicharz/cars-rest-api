import json

from django.test import TestCase, Client
from django.urls import reverse

from cars_rest.models import Car
from cars_rest.tests import helpers


class TestRatesView(TestCase):
    def setUp(self) -> None:
        car = Car(
            make='Honda',
            model='Civic',
        )
        car.save()

        self.rates = helpers.generate_rates_post_data(car)
        self.client = Client()
        self.expected_fields = {
            'car_id': int,
            'rating': int,
        }

    def test_request_post_on_rates_view(self):
        ratings = []

        # Valid ratings
        for rate in self.rates[:2]:
            response = self.client.post(
                reverse('rates'),
                data=json.dumps(rate),
                content_type='application/json'
            )

            self.assertNotEqual(response.status_code, 500)
            self.assertEqual(response.status_code, 201)

            for k, v in response.data.items():
                self.assertIn(k, self.expected_fields.keys())
                self.assertIsInstance(v, self.expected_fields[k])

            ratings.append(rate['rating'])

        # Invalid ratings
        for rate in self.rates[3:5]:
            response = self.client.post(
                reverse('rates'),
                data=json.dumps(rate),
                content_type='application/json'
            )

            self.assertNotEqual(response.status_code, 500)
            self.assertNotEqual(response.status_code, 201)
            self.assertEqual(response.status_code, 400)

        response = self.client.get(reverse('cars'))
        car = response.data[0]

        average_rating = helpers.count_average(ratings)

        self.assertEqual(car['avg_rating'], average_rating)
