from django.test import TestCase, Client
from django.urls import reverse

from cars_rest.tests import helpers


class TestPopularView(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        for car in helpers.generate_cars():
            car.save()
            car_rates = helpers.generate_rates(car)
            car.rates_number = len(car_rates)
            car.save()
            [rate.save() for rate in car_rates]

    def test_request_get_on_popular_view(self):
        response = self.client.get(reverse('popular'))

        self.assertNotEqual(response.status_code, 404)
        self.assertEqual(response.status_code, 200)

        data = response.data

        self.assertEqual(
            data,
            sorted(data, key=lambda obj: obj['rates_number'], reverse=True)
        )
