from rest_framework.generics import ListAPIView

from cars_rest.models import Car
from cars_rest.serializers.popular_serializer import PopularSerializer


class PopularView(ListAPIView):
    queryset = Car.objects.all().order_by('-rates_number')
    serializer_class = PopularSerializer
