from rest_framework.generics import ListCreateAPIView, DestroyAPIView

from ..models import Car
from ..serializers import CarSerializer


class CarsView(ListCreateAPIView, DestroyAPIView):
    queryset = Car.objects.all()
    serializer_class = CarSerializer
