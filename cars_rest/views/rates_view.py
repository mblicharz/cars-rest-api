from django.db import transaction
from rest_framework.generics import CreateAPIView
from rest_framework import status

from ..models import Rate
from ..serializers.rate_serializer import RateSerializer
from ..services import rates_service


class RatesView(CreateAPIView):
    queryset = Rate.objects.all()
    serializer_class = RateSerializer

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        response = self.create(request, *args, **kwargs)
        if response.status_code == status.HTTP_201_CREATED:
            rates_service.update_rating_statistics_for_car(
                response.data['car_id']
            )
        return response
