from rest_framework import serializers

from cars_rest.models import Car


class PopularSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = (
            'id',
            'make',
            'model',
            'rates_number',
        )
