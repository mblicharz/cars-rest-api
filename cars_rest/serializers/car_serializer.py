from rest_framework import serializers
from rest_framework.exceptions import NotFound

from ..exceptions import ServiceUnavailable
from ..models import Car
from ..nhtsa_api_provider import get_model, NHTSAUnexpectedData, CarNotFound, \
    NHTSAUnavailable


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = (
            'id',
            'make',
            'model',
            'avg_rating',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'avg_rating',
            'created_at',
            'updated_at'
        )

    def validate(self, attrs):
        try:
            model = get_model(attrs['make'], attrs['model'])
        except NHTSAUnavailable:
            raise ServiceUnavailable(
                detail='NHTSA service is unavailable.'
            )
        except NHTSAUnexpectedData:
            raise ServiceUnavailable(
                detail='NHTSA service returned unexpected data'
            )
        except CarNotFound:
            raise NotFound(detail="Given car model was not found")

        attrs['make'], attrs['model'] = model
        return attrs
