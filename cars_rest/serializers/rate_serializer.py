from rest_framework import serializers

from ..models import Rate, Car


class RateSerializer(serializers.ModelSerializer):
    car_id = serializers.PrimaryKeyRelatedField(queryset=Car.objects.all())

    class Meta:
        model = Rate
        fields = (
            'car_id',
            'rating'
        )

    def create(self, validated_data):
        rate_obj = Rate(
            car=validated_data['car_id'],
            rating=validated_data['rating']
        )
        rate_obj.save()
        return validated_data
