FROM python:3.6.9-alpine

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
COPY requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt

RUN mkdir /app
COPY . /app/
WORKDIR /app

CMD sh -c "python manage.py migrate && python manage.py runserver 0.0.0.0:$PORT"
