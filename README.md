**Setup**
1. I recommend you to use docker-compose: `docker-compose up --build api`
2. Application should work on `0.0.0.0:8000`
3. To run tests: `docker-compose up --build tests`

If you experience any issues, feel free to report them to me :)

***

**Environment variables**
- `DATABASE_URL` - Url to connect with db
- `DJANGO_SETTINGS_MODULE` - Path to django settings eg. 'cars.settings'
- `DEBUG` - debug mode

***


**API**
- GET '/cars' - List of cars stored in the database.
- POST '/cars' - Create a new car.
<div style="padding-left: 30px">
<details>
<summary>body</summary>

```json
{
  "make": "fiat",
  "model": "brava"
}
```
</details>
</div>

- DELETE '/cars/id' - Delete a car with specified id.
- POST '/rate' - Create new rate.

<div style="padding-left: 30px">
<details>
<summary>body</summary>

```json
{
  "car_id": 1,
  "rating": 5
}
```
</details>
</div>

- GET '/popular' - Get list of cars ordered by rating count.
